//
//  main.c
//  TAD_C
//
//  Created by Santiago Hazana on 3/19/17.
//  Copyright © 2017 SH. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "ComplexNumber.h"

int main() {
    
    printf("Trabajo practico TAD en C - Algoritmos y estructura de datos\n Integrantes:\n -Santiago Hazaña\n -Lucas Manzanelli\n ------------------\n");
    
    double a, b, c, d;
    printf("Please insert 4 doubles and values for the complex number\n");
    printf("Real part of number A: ");
    scanf("%lf", &a);
    printf("Complex part of number A: ");
    scanf("%lf", &b);
    printf("Real part of number B: ");
    scanf("%lf", &c);
    printf("Complex part of number B: ");
    scanf("%lf", &d);
    
    ComplexNumber *numberA = createComplexNumber(&a, &b);
    ComplexNumber *numberB = createComplexNumber(&c, &d);
    
    
    printf("\nAddition: between");
    print(numberA);
    printf(" and ");
    print(numberB);
    printf(" = ");
    print(addition(numberA, numberB));
    printf("\n\n");
    
    printf("Substraction: between");
    print(numberA);
    printf(" and ");
    print(numberB);
    printf(" = ");
    print(substract(numberA, numberB));
    printf("\n\n");
    
    printf("Multiplication: between");
    print(numberA);
    printf(" and ");
    print(numberB);
    printf(" = ");
    print(multiplication(numberA, numberB));
    printf("\n\n");
    
    printf("Division: between");
    print(numberA);
    printf(" and ");
    print(numberB);
    printf(" = ");
    print(division(numberA, numberB));
    printf("\n\n");
    
    printf("Module of: ");
    print(numberA);
    printf(" = ");
    printf("%2f", *module(numberA));
    printf("\n\n");
    
    printf("Module of: ");
    print(numberB);
    printf(" = ");
    printf("%2f", *module(numberB));
    printf("\n\n");

    freeSpace(numberA);
    freeSpace(numberB);
    
    return 0;
}
