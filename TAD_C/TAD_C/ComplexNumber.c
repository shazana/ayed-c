//
//  ComplexNumber.c
//  TAD_C
//  Created by Santiago Hazana on 3/19/17.
//  Copyright © 2017 SH. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ComplexNumber.h"

/*
 Function: createComplexNumber
 Description: allocates memory for a complex number and all it's components.
 Returns: ComplexNumber pointer
 */

ComplexNumber* createComplexNumber(double* realPart, double* complexPart){
    
    /*
     Allocates memory for the complex number and it's variables
     */    
    ComplexNumber* result = malloc(sizeof(ComplexNumber));
    result->realPart = malloc(sizeof(double));
    result->complexPart = malloc(sizeof(double));
    
    /*
     Copies the value of real and complex part to the allocated memory
     */
    *result->realPart = *realPart;
    *result->complexPart = *complexPart;
    
    return result;
    
}

/*
 Function: addition
 Description: calculates the addition of two complex numbers
 Returns: ComplexNumber pointer that points to the result
 */

ComplexNumber* addition(ComplexNumber* numberA, ComplexNumber* numberB){
    double newRealPart = *numberA->realPart + *numberB->realPart;
    double newComplexPart = *numberA->complexPart + *numberB->complexPart;
    ComplexNumber* result = createComplexNumber(&newRealPart, &newComplexPart);
    return result;
}

/*
 Function: substract
 Description: calculates the substraction of two complex numbers
 Returns: ComplexNumber pointer that points to the result
 */

ComplexNumber* substract(ComplexNumber* numberA, ComplexNumber* numberB){
    double newRealPart = *numberA->realPart - *numberB->realPart;
    double newComplexPart = *numberA->complexPart - *numberB->complexPart;
    ComplexNumber* result = createComplexNumber(&newRealPart, &newComplexPart);
    return result;
}

/*
 Function: multiplication
 Description: calculates the multiplication of two complex numbers
 Returns: ComplexNumber pointer that points to the result
 */


ComplexNumber* multiplication(ComplexNumber* numberA, ComplexNumber* numberB){
    double newRealPart = (*numberA->realPart * *numberB->realPart) - (*numberA->complexPart * *numberB->complexPart);
    double newComplexPart = (*numberA->realPart * *numberB->complexPart) + (*numberA->complexPart * *numberB->realPart);
    ComplexNumber* result = createComplexNumber(&newRealPart, &newComplexPart);
    return result;
}

/*
 Function: division
 Description: calculates the division of two complex numbers
 Returns: ComplexNumber pointer that points to the result
 */

ComplexNumber* division(ComplexNumber* numberA, ComplexNumber* numberB){
    double newRealPart = ((*numberA->realPart * *numberB->realPart) + (*numberA->complexPart * *numberB->complexPart)) / (pow(*numberB->realPart, 2) + pow(*numberB->complexPart, 2));
    double newComplexPart = ((*numberA->complexPart * *numberB->realPart) - (*numberA->realPart * *numberB->complexPart)) / (pow(*numberB->realPart, 2) + pow(*numberB->complexPart, 2));
    ComplexNumber* result = createComplexNumber(&newRealPart, &newComplexPart);
    return result;
}

/*
 Function: module
 Description: prinsts the complex number
 Returns:
 */

double* module(ComplexNumber* number){
    double result = sqrt(pow(*number->realPart, 2) + pow(*number->complexPart, 2));
    return &result;
}

/*
 Function: print
 Description: prinsts the complex number
 Returns:
 */

void print(ComplexNumber* numberA){
    printf(" %.2f + %.2fi", *numberA->realPart, *numberA->complexPart);
}

/*
 Function: freeSpace
 Description: frees the space used by a given complex number
 Returns:
 */

void freeSpace(ComplexNumber* number){
    
    free (number->realPart);
    free (number->complexPart);
    free (number);
}
