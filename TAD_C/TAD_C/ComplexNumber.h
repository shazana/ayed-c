//
//  ComplexNumber.h
//  TAD_C
//
//  Created by Santiago Hazana on 3/19/17.
//  Copyright © 2017 SH. All rights reserved.
//

#ifndef ComplexNumber_h
#define ComplexNumber_h

#include <stdio.h>

typedef struct complexNumber ComplexNumber;

struct complexNumber{
    double* realPart;
    double* complexPart;
};

ComplexNumber* createComplexNumber(double* realPart, double* complexPart);
ComplexNumber* addition(ComplexNumber* numberA, ComplexNumber* numberB);
ComplexNumber* substract(ComplexNumber* numberA, ComplexNumber* numberB);
ComplexNumber* multiplication(ComplexNumber* numberA, ComplexNumber* numberB);
ComplexNumber* division(ComplexNumber* numberA, ComplexNumber* numberB);
double* module(ComplexNumber* number);
void print(ComplexNumber* numberA);
void freeSpace(ComplexNumber* complexNumber);

#endif /* ComplexNumber_h */
